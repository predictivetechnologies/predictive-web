marked  = require('marked')
Promise = require('promise')

token = null

loadWelcome = () ->
  new Promise (resolve, reject) ->
    handleCorrect = (result) ->
      resolve marked(result)

    handleProblem = (error) ->
      reject(error)

    reqwest('/welcome.md').then(handleCorrect, handleProblem)

buildPredictiveApi = (host, version) ->
  {
    register: (loginData) ->
      service = @
      new Promise (resolve, reject) ->
        reqwest({
          url: "#{host}/api/#{version}/auth/register",
          data: JSON.stringify(loginData),
          type: 'json',
          contentType: 'application/json',
          method: 'post'
        }).then (data) ->
          token = data.token if(data.token)
          resolve(data)
        , (data) ->
          reject(data)
    login: (loginData) ->
      service = @
      new Promise (resolve, reject) ->
        reqwest({
          url: "#{host}/api/#{version}/auth/login",
          data: JSON.stringify(loginData),
          type: 'json',
          contentType: 'application/json',
          method: 'post'
        }).then (data) ->
          token = data.token if(data.token)
          resolve(data)
        , (data) ->
          reject(data)
    listQuestions: () -> reqwest("#{host}/api/#{version}/questions")
    finishUrl: (data) ->
      reqwest
        headers:
          'X-Auth-Token': token
        url: "#{host}/api/#{version}/questionnaire/finishUrl"
        data: JSON.stringify(data),
        type: 'json',
        contentType: 'application/json',
        method: 'post'
    saveFeedback: (data) ->
      reqwest
        headers:
          'X-Auth-Token': token
        url: "#{host}/api/#{version}/feedback/save"
        data: JSON.stringify(data),
        type: 'json',
        contentType: 'application/json',
        method: 'post'
    saveQuestionnaire: (data) ->
      reqwest
        headers:
          'X-Auth-Token': token
        url: "#{host}/api/#{version}/questionnaire/save"
        data: JSON.stringify(data),
        type: 'json',
        contentType: 'application/json',
        method: 'post'
    randomUrl: () ->
      reqwest
        headers:
          'X-Auth-Token': token
        url: "#{host}/api/#{version}/urls/random"
  }

module.exports =
  loadWelcome: loadWelcome
  buildPredictiveApi: buildPredictiveApi
  api: buildPredictiveApi 'http://api.ediaz.me', 'v1'
  # api: buildPredictiveApi 'http://localhost:8080', 'v1'
