# Bring in jQuery and React as a Bower component in the global namespace
require("script!jquery/dist/jquery.js")
require("script!react/react-with-addons.js")
require("script!bootstrap/dist/js/bootstrap.js")
require("script!aviator/aviator.js")
require("script!reqwest/reqwest.js")
require("script!lodash/dist/lodash.js")
require("script!aviator/aviator.js")
require("script!postal.js/lib/postal.js")
require("./jquery.backtotop.js")
Question = require("./sidebar.coffee").Question
Canvas   = require("./canvas.coffee").Canvas
request  = require("./request.coffee")

{div} = React.DOM

channel = postal.channel()
dp = jQuery
dp.noConflict()

api = request.api

questions = null

Main = React.createClass
  getInitialState: ->
    {
      fullCanvas: true
    }

  render: ->
    (div {
      style: {
        height: '100%'
      }
    }, [
      (Question {}),
      (Canvas {})
    ])

RoutesTarget = {
  loadWelcome: ->
    channel.publish('route.general', {
      route: 'welcome',
      full: true
    })
  loadFinished: ->
    channel.publish('route.general', {
      route: 'finished',
      full: true
    })
  loadEvaluate: ->
    api.randomUrl().then (data) ->
      urls = data.urls
      if(urls.length > 0)
        channel.publish('route.general', {
          route: 'evaluation',
          full: false,
          url: urls[0].url,
          questions: questions,
          answers: data.answers
        })
      else
        Aviator.navigate('/finished')

}

Aviator.pushStateEnabled = false
Aviator.setRoutes {
  target: RoutesTarget,
  '/welcome':  'loadWelcome',
  '/evaluate': 'loadEvaluate'
  '/finished': 'loadFinished'
}


dp(document).ready ->
  dp("#backtotop").backToTop()
  dp("#backtotop").click () ->
    window.scrollTo 0, 0
    false


window.fbAsyncInit = () ->
  FB.init {
    appId  : '453196654825636', #prod
    # appId  : '1391983497689071', #dev
    status : true,
    cookie : true,
    xfbml  : true,
    oauth  : true
  }

  dp("#main-wrapper").fadeIn 100, "linear"
  dp("#loader").fadeOut 100, "linear"

  api.listQuestions().then (data) ->
    questions = data
    React.renderComponent (Main {}), document.getElementById('main-wrapper')
    Aviator.dispatch()
    FB.getLoginStatus (response) ->
      if(response.status == 'connected')
        api.login(response.authResponse).then (data) ->
          console.log Aviator.getCurrentURI()
          if Aviator.getCurrentURI() == ''
            Aviator.navigate('/welcome')
            # Aviator.navigate('/finished')
          else
            Aviator.navigate(Aviator.getCurrentURI())
      else
        Aviator.navigate('/welcome')


dp(window).load ->


