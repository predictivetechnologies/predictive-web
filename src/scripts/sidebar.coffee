_       = require('lodash')
request = require('./request.coffee')

summaryEvery = 2
api = request.api

{div, h4, h5, ul, li, span, p, a, button, input, label, br, i} = React.DOM

channel = postal.channel()

refreshState = null
module.exports.Question = React.createClass
  getInitialState: ->
    {shown: false, questions: []}

  prevQuestion: ->
    questionIdx = @state.questionIdx
    questions   = @state.questions

    if(questionIdx > 0)
      question = questions[@state.questionIdx - 1]
      @setState {
        questionIdx: @state.questionIdx - 1,
        enableNext: _.any question.possibilities, (it) ->
          it.checked
        question: question,
        questions: @state.questions,
        shown: @state.shown,
        url: @state.url
      }

  nextQuestion: ->
    if not @state.enableNext
      return

    questionIdx = @state.questionIdx
    questions   = @state.questions

    questionSummary = _(questions).map (q) ->
      {
        id: q.id,
        possibilities: _(q.possibilities).filter (it) ->
          it.checked
        .map (it) ->
          it.id
        .value()
      }
    .value()


    urlData =
      questionSummary: questionSummary
      url: @state.url

    invalidContent = =>
      @state.questionIdx <= 2 && _.any @state.question.possibilities, (it) ->
        it.checked && (it.possibility.toLowerCase() == 'no' || it.possibility.toLowerCase() == 'the resource is spread in several pages')

    success = =>
      if(questionIdx < questions.length - 1 and not invalidContent())
        question = questions[@state.questionIdx + 1]
        @setState {
          questionIdx: @state.questionIdx + 1,
          enableNext: _.any question.possibilities, (it) ->
            it.checked
          question: question,
          questions: @state.questions,
          shown: @state.shown,
          url: @state.url
        }
      else
        that = this
        api.finishUrl({url: @state.url}).then ->
          api.randomUrl().then (data) ->
            urls = data.urls
            if(urls.length == 0 || data.answered % summaryEvery == 0)
              Aviator.navigate('/finished')
            else
              channel.publish('route.general', {
                route: 'evaluation',
                enableNext: false,
                full: false,
                url: urls[0].url,
                questions: that.state.questions
              })

      scrollToTop = () ->
        finishedScrolling = ->
          jQuery("#sidebar").animate({
            scrollTop: 2
          }, 100)

        jQuery("#sidebar").animate({
          scrollTop: jQuery('#sidebar').offset().top
        }, 10, finishedScrolling)

      scrollToTop()

    api.saveQuestionnaire(urlData).then success

  componentDidMount: ->
    that = this

    refreshState = () ->
      that.setState {
        questionIdx: that.state.questionIdx,
        question: that.state.question,
        enableNext: _.any that.state.question.possibilities, (it) ->
          it.checked
        questions: that.state.questions,
        shown: that.state.shown,
        url: that.state.url
      }

    channel.subscribe 'route.general', (data) ->
      shown = data.route != 'welcome' && data.route != 'finished'
      question = if(data.questions?.length > 0)
        data.questions[0]

      possibilities = _(data.questions).map (it) ->
        it.possibilities
      .flatten()
      .value()

      answers = []
      if data.answers
        answers = _(data.answers).map (it) ->
          it.possibilities
        .flatten()
        .value()

      _.each possibilities, (it) ->
        it.checked = _.contains answers, it.id
        true


      _.each data.questions, (question) ->
        if data.answers
          answers = data.answers[question.id]

      isAnswered = (question) ->
        _.any question.possibilities, (it) ->
          it.checked

      questionIdx = 0

      if(data.questions?.length > 0)
        questionIdx += 1 while isAnswered(data.questions[questionIdx])
        question = data.questions[questionIdx]

      questions = data.questions

      that.setState {
        questionIdx: questionIdx,
        question: question,
        questions: questions,
        enableNext: question and isAnswered(question)
        shown: shown,
        url: data.url
      }


  render: ->
    style = {
      'overflow-y': 'auto',
      'padding-right': '10px'
    }
    if(!@state.shown)
      style.display = 'none'
    that = @
    question = @state.question
    type = if(@state.question?.multivalued)
      'checkbox'
    else
      'radio'
    (div {id: 'sidebar', style: style}, [
      (div {className: 'navbar navbar-default'}, [
        (div {className: 'navbar-collapse'}, [
          (div {className: 'pull-right', style: {'padding-top': '18px'}}, [
            (a {onClick: @prevQuestion, style: {cursor: 'pointer', color: '#232a2f'}}, [
              (i {className: 'fa fa-angle-left', style: {'font-size': '40px'}})
            ])
            (a {disabled: not @state.enableNext, onClick: @nextQuestion, style: {cursor: 'pointer', color: '#232a2f', 'margin-left': '10px'}}, [
              (i {className: 'fa fa-angle-right', style: {'font-size': '40px'}})
            ])
          ])
          (div {}, [
            (h4 {}, @state.question?.question)
          ])
          (span {style: {'font-weight': 'lighter', 'font-size': '12px'}}, 'Please select the answers that you may deem more appropriate to the question posed to you. In some questions you may select multiple answers.')
          (ul {className: 'nav navbar-nav questions'}, [
            _.map @state.question?.possibilities, (it) ->
              changeValue = (val) ->
                unknown = it.id == 0 && !it.checked
                if(type == 'radio' or unknown)
                  _.each question.possibilities, (it) ->
                    it.checked = false
                    true
                  it.checked = true
                else
                  _.each question.possibilities, (it) ->
                    it.checked = false if(it.id == 0)
                    true
                  it.checked = not it.checked
                refreshState()
                if type == 'radio'
                  loadNext = () -> that.nextQuestion()
                  _.delay(loadNext, 100)
              (li {}, [
                (a {}, [
                  (label {style: {'font-size': '12px'}}, [
                    (input {name: 'answer', onChange: changeValue, value: it.id, checked: it.checked, type: type, style: {'margin-right': '10px'}})
                    it.possibility
                  ])
                ])
              ])
          ])
          (button {disabled: not @state.enableNext, style: {'margin-left': '25%'}, className: 'btn btn-light', onClick: @nextQuestion }, ['Continue'])
          (br {})
          (br {})
          (div {}, [
            (h5 {}, 'Detailed instructions')
            (span {style: {'font-weight': 'lighter', 'font-size': '14px'}}, @state.question?.explanation)
          ])
          (br {})
          (br {})
        ])
      ])
    ])

