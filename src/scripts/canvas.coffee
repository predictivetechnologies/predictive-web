request = require("./request.coffee")

api = request.api

{a, div, text, iframe, h2, h3, h4, button, section, p, br, form, input, label, textarea} = React.DOM

if (typeof String::endsWith != 'function')
  String::endsWith = (str) ->
    return this.slice(-str.length) == str

channel = postal.channel()

onChange = (that) ->
  (evt) ->
    feedback.body += evt.key

load = ->
  jQuery('#overlay').remove()
  window.scrollTo 0, 0

goOn = ->
  Aviator.navigate('/evaluate')

showRegistry = (that) ->
  ->
    that.state.showRegistry = !that.state.showRegistry
    that.setState that.state

sendFeedback = (that) ->
  ->
    state = that.state
    feedback =
      body: state.body
      name: state.name
      email: state.email
      subject: state.subject
    api.saveFeedback(feedback).then ->
      that.setState {
        content: null,
        finished: true,
        full: that.state.full,
        moreUrls: that.state.moreUrls,
        body: '',
        name: '',
        email: '',
        subject: '',
        url: null
      }

startLoading = ->
  over = '<div id="overlay"><img id="loading" src="loading4.gif"></div>'
  jQuery(over).appendTo('body')

login = (that) ->
  ->
    username = that.state.username
    password = that.state.password
    loginData =
      username: username
      password: password

    api.login(loginData).then (data) ->
      Aviator.navigate('/evaluate')

register = (that) ->
  ->
    username = that.state.username
    password = that.state.password
    loginData =
      username: username
      password: password

    api.register(loginData).then (data) ->
      Aviator.navigate('/evaluate')



loginWithFacebook =  ->
  loggedIn = (response) ->
    if(response.authResponse)
      api.login(response.authResponse).then (data) ->
        Aviator.navigate('/evaluate')
    else
      console.log 'Unable to authenticate'

  FB.login(loggedIn, {scope:'publish_actions'})

module.exports.Canvas = React.createClass
  mixins: [React.addons.LinkedStateMixin],
  getInitialState: ->
    {
      content: '',
      full: true,
      finished: false,
      url: null,
      body: '',
      name: '',
      email: '',
      subject: ''
    }

  loadWelcome: ->
    that = this
    request.loadWelcome().then (data) ->
      that.setState {
        content: data,
        finished: false,
        full: that.state.full,
        body: '',
        name: '',
        email: '',
        subject: '',
        url: null
      }

  loadFinished: ->
    that = this
    api.randomUrl().then (data) ->
      that.setState {
        content: null,
        finished: true,
        full: that.state.full,
        moreUrls: data.urls.length > 0,
        body: '',
        name: '',
        email: '',
        subject: '',
        url: null
      }

  componentDidMount: ->
    that = this
    jQuery('iframe').load(load)
    channel.subscribe 'route.general', (data) ->
      startLoading() if data.url and not data.url.endsWith('.pdf')
      that.setState {
        content: that.state.content,
        url: data.url,
        full: data.full,
        body: '',
        name: '',
        email: '',
        subject: '',
        finished: data.finished
      }
      if(data.route == 'welcome')
        that.loadWelcome()
      if(data.route == 'finished')
        that.loadFinished()

  render: ->
    # style = {'overflow-y': 'auto'}
    style = {}
    if @state.url and @state.url.endsWith(".pdf")
      clazz = 'full-pdf-size'
      window.scrollTo 0, 0
    else
      clazz = 'full-size'
    cl = if(@state.full)
      'left-container'
    else
      'container'
    (div {id: 'container', className: cl, style: style}, [
      (div {
        className: 'full-height',
        style: {
          'overflow-wrap': 'break-word',
          display:
            if(@state.url)
              'block'
            else
              'none'
        }
      }, [
         (iframe {
           className: clazz
           src: @state.url})
      ])
      (section {
        style: {
          display:
            if(@state.finished)
              'block'
            else
              'none'
        }
      }, [
        (div {className: 'row'}, [
          (div {className: 'col-md-6'}, [
            (div {className: 'header-content'}, [
              (h2 {}, [
                (text {}, 'Thank you!')
              ])
            ])
            (div {}, [
              (p {}, 'Your answers will be tremendously useful for this research project. You’ll hear back from us soon. Cheers,')
              (br {})
              (p {}, 'Predictive Technologies Team')
              (div {className: 'row'}, [
              ])
            ])
          ])
          (div {className: 'col-md-6', style: {display: 'block'} }, [
            (div {style:
                display:
                  if true
                    'block'
                  else
                    'none'}, [
              (p {}, ['If you want to continue helping you can go on!'])
              (button {className: 'btn btn-light big', onClick: goOn }, ['Continue'])
            ])
            (br {})
            (br {})
            (p {}, ['Please send your feedback and help us improve'])
            (form {}, [
              (div {className: 'form-group'}, [
                (label {}, ['Name'])
                (input {type: 'text', className: 'form-control', valueLink: @linkState('name')}, [])
              ])
              (div {className: 'form-group'}, [
                (label {}, ['Email'])
                (input {type: 'email', className: 'form-control', valueLink: @linkState('email')}, [])
              ])
              (div {className: 'form-group'}, [
                (label {}, ['Subject'])
                (input {type: 'text', className: 'form-control', valueLink: @linkState('subject')}, [])
              ])
              (div {className: 'form-group'}, [
                (label {}, ['Body'])
                (textarea {className: 'form-control', valueLink: @linkState('body') }, [])
              ])
              (button {type: 'submit', className: 'btn btn-light big', onClick: sendFeedback(@) }, ['Send feedback'])
            ])
          ])
        ])
      ])
      (section {
        className: 'odd'
        style: {
          display:
            if(@state.url || @state.finished)
              'none'
            else
              'block'
        }
      }, [
        (div {className: 'row'}, [
          (div {className: 'col-md-12'}, [
            (div {className: 'header-content'}, [
              (h2 {}, ['Welcome teacher'])
            ])
            (div {
              dangerouslySetInnerHTML: {
                __html: @state.content
              }
            })
          ])
        ])
      ])
      (section {
        className: 'call-ta'
        style: {
          display:
            if(@state.url || @state.finished)
              'none'
            else
              'block'
        }
      }, [
        (div {className: 'row'}, [
          (div {className: 'col-sm-12'}, [
            (div {className: 'col-md-7'}, [
              (p {}, [
                'If you don´t have an account yet and you don´t want to use facebook, you can register '
                (a {onClick: showRegistry(@)}, ['here'])
              ])
              (form {
                style: {
                  display:
                    if(!@state.showRegistry)
                      'none'
                    else
                      'block'
                }
              }, [
                (div {className: 'form-group'}, [
                  (label {}, ['Username'])
                  (input {type: 'text', className: 'form-control', valueLink: @linkState('username')}, [])
                ])
                (div {className: 'form-group'}, [
                  (label {}, ['Password'])
                  (input {type: 'password', className: 'form-control', valueLink: @linkState('password')}, [])
                ])
                (button {className: 'btn btn-light big', onClick: register(@) }, ['Register'])
              ])
            ])
            (div {className: 'col-md-5'}, [
              (form {}, [
                (div {className: 'form-group'}, [
                  (label {}, ['Username'])
                  (input {type: 'text', className: 'form-control', valueLink: @linkState('username')}, [])
                ])
                (div {className: 'form-group'}, [
                  (label {}, ['Password'])
                  (input {type: 'password', className: 'form-control', valueLink: @linkState('password')}, [])
                ])
                (button {className: 'btn btn-light big', onClick: login(@) }, ['Login'])
              ])
              (button {className: 'btn btn-light big', onClick: loginWithFacebook }, ['Login with Facebook'])
            ])
          ])
        ])
        (div {className: 'row'}, [
          (div {className: 'col-md-offset-7 col-md-5'}, [
            (div {}, [
              (h3 {}, [
                (text {}, 'Thank you for helping us')
              ])
              (h4 {}, [
                (text {}, 'Your insights are important to the world')
              ])
            ])
          ])
        ])
      ])
    ])


